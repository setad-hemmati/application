import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  RefreshControl,
  Alert,
} from 'react-native';

import {CardComp} from '.';

export const NewsScreen = () => {
  const [news, setNews] = useState([]);
  const [loading, setLoading] = useState(false);
  const loadNews = () => {
    setLoading(true);
    fetch('https://hemmati1400.github.io/news.json')
      .then(res => res.json())
      .then(
        result => {
          setNews(result);
          setLoading(false);
        },
        error => {
          Alert.alert('Something wrong');
          setNews([]);
          setLoading(false);
        },
      );
  };
  useEffect(() => {
    loadNews();
  }, []);
  return (
    <ScrollView
      contentInsetAdjustmentBehavior="automatic"
      refreshControl={
        <RefreshControl refreshing={loading} onRefresh={loadNews} />
      }
      contentContainerStyle={styles.container}>
      <StatusBar barStyle={'light-content'} />
      {news.map((item, index) => (
        <CardComp
          key={index}
          title={item.title}
          description={item.description}
          imgSrc={item.imgSrc}
          openUrl={item.openUrl}
        />
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    marginRight: 15,
    marginLeft: 15,
  },
  vazir: {
    fontFamily: 'Vazir',
  },
  vazirBold: {
    fontFamily: 'Vazir-Bold',
  },
});
